'''https://hands-on.cloud/working-with-secrets-manager-in-python-using-boto3/#Retrieve-a-secret-values-from-the-Python-code'''

import boto3
import json

client = boto3.client('secretsmanager')

response = client.get_secret_value(
    SecretId='TestSecret'
)

database_secrets = json.loads(response['SecretString'])
print(database_secrets['username'])
print(database_secrets['password'])



