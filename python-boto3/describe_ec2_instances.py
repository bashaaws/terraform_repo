import boto3

AWS_REGION = "eu-west-1"
EC2_CLIENT = boto3.client('ec2', region_name=AWS_REGION)

# either hard code instance ID or use input() to dynamically access input
INSTANCE_ID = 'i-04dbb5d2a9b59f35b'


response = EC2_CLIENT.describe_instances(
    InstanceIds=[
        INSTANCE_ID
    ],
)

print(response)