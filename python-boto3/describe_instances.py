import boto3

client = boto3.client('ec2', region='us-east-1')
response = client.describe_instances()
print(response)

for instance in response['Reservations']:
    print("InstanceId is {} ".format(instance['InstanceId']))
