import boto3
region = 'us-east-1'

ec2_client = boto3.client('ec2', region_name=region)

print('Into DescribeEc2Instance')
response = ec2_client.describe_instances()

print(response)
