terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "2.11.0"
    }
#    mysql = {
#      source = "kaplanmaxe/mysql"
#      version = "1.9.5"
#    }
  }
}

#provider "mysql" {
#  endpoint = "172.17.0.2:3306"
#  username = "root"
#  password = "test1234"
#}


resource "docker_image" "mysql" {
  name = "mysql:5.7"
}

resource "docker_image" "wordpress" {
  name = "wordpress:latest"
}

resource "random_password" "mysql_root_password" {
  length = 10
}

resource "random_password" "mysql_wordpress_password" {
  length = 10
}
resource "docker_container" "mysql" {
  name = "mysql"
  image = docker_image.mysql.latest
  env   = [
    "MYSQL_ROOT_PASSWORD=test1234"
  ]

  volumes {
    container_path  = "/var/lib/mysql"
    # replace the host_path with full path for your project directory starting from root directory /
    host_path = "/some/host/mysql/data/path"
    read_only = false
  }
  ports {
    internal = 3306
    external = 3306
  }
  restart = "always"
}

# create wordpress container
resource "docker_container" "wordpress" {
  name  = "wordpress"
  image = docker_image.wordpress.latest
  restart = "always"

  ports {
    internal = "80"
    external = "80"
  }

}

#resource "mysql_database" "app" {
#  name = "wordpress"
#}

