variable "allowed_cidr_blocks" {
  type = list(string)
  default = ["0.0.0.0/0", "10.0.0.0/8"]
}

variable "subnets" {
  type = list(string)
  default = [ "subnet-c0e5e5a6", "subnet-7b551821"]
}

variable "vpc_id" {
  type = string
  default = "vpc-e66fb89f"
}