resource "aws_kms_key" "example" {
  description             = "example"
  deletion_window_in_days = 10
  policy = "${file("kms-policy.json")}"
}

resource "aws_kms_key" "example1" {
  description             = "example"
  deletion_window_in_days = 10
  policy = "${file("kms-policy.json")}"
}